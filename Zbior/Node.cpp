/*
#ifndef NODE_CPP
#define NODE_CPP
#include "Node.h"
#include "stdafx.h"
#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <string>
using namespace std;



template<class T>
Node<T>::Node(){
}

template<class T>
Node<T>::~Node(){
}

template<class T>
Node<T>::Node(Node<T>* parent, T value){
	this->setParent(parent);
	this->setValue(value);
	this->_larger=nullptr;
	this->_smaller=nullptr;
}

template<class T>
T Node<T>::getValue(){
	return this->_value;
}

template<class T>
void Node<T>::setValue(T value){
	this->_value=value;
}

template<class T>
Node<T>* Node<T>::getParent(){
	return this->_parent;
}

template<class T>
void Node<T>::setParent(Node<T>* parent){
	this->_parent=parent;
}

template<class T>
Node<T>* Node<T>::getSmaller(){
	return this->_smaller;
}

template<class T>
void Node<T>::setSmaller(Node<T>* node){
	this->_smaller=node;
}

template<class T>
Node<T>* Node<T>::getLarger(){
	return this->_larger;
}

template<class T>
void Node<T>::setLarger(Node<T>* node){
	this->_larger=node;
}

template<class T>
bool Node<T>::isSmaller(){
	return this->getParent() != nullptr && this==this->getParent()->getSmaller();
}

template<class T>
bool Node<T>::isLarger(){
	return this->getParent() != nullptr && this==this->getParent()->getLarger();
}

template<class T>
Node<T>* Node<T>::minimum(){
	Node<T>* node = this;

	while(node->getSmaller() != nullptr)
		node = node->getSmaller();

	return node;
}

template<class T>
Node<T>* Node<T>::maximum(){
	Node<T>* node = this;
	
	while(node->getLarger() != nullptr)
		node = node->getLarger();

	return node;
}

template<class T>
Node<T>* Node<T>::successor(){
	if(this->getLarger() != nullptr)
		return this->getLarger()->minimum();

	Node<T>* node = this;

	while (node->isLarger())
		node = node->getParent();

	return node->getParent();
}

template<class T>
Node<T>* Node<T>::predecessor(){
	if(this->getSmaller() != nullptr)
		return this->getSmaller()->maximum();

	Node<T>* node = this;

	while (node->isSmaller())
		node = node->getParent();

	return node->getParent();
}
#endif
*/