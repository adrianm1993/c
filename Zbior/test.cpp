#ifndef TEST_H
#define TEST_H

#include "Node.h"
#include "TreeSet.h"
#include <iostream>
#include <ostream>
#include <string>
using namespace std;

void main(){
	int jeden = 1;
	int dwa = 10;
	int trzy = 15;
	int cztery = 5;
	int piec = 44;
	
	TreeSet<int> ts;
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	ts.add(jeden);
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	ts.add(dwa);
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	ts.add(trzy);
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	ts.add(cztery);
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	
	cout << "Czy zawiera 1:" << ts.contains(jeden) << endl;
	cout << "Czy zawiera 10:" << ts.contains(dwa) << endl;
	cout << "Czy zawiera 15:" << ts.contains(trzy) << endl;
	cout << "Czy zawiera 5:" << ts.contains(cztery) << endl;
	cout << "Czy zawiera 44:" << ts.contains(piec) << endl;
	
	cout << "Czy pusty:" << ts.isEmpty() << endl;

	cout << "Usuwanie 2:" << ts.deletee(dwa) << endl;
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	cout << "Usuwanie 1:" << ts.deletee(jeden) << endl;
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	cout << "Usuwanie 2:" << ts.deletee(dwa) << endl;
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	cout << "Czy zawiera 1:" << ts.contains(jeden) << endl;
	cout << "Czy zawiera 10:" << ts.contains(dwa) << endl;
	cout << "Usuwanie 3:" << ts.deletee(trzy) << endl;
	cout << "Usuwanie 4:" << ts.deletee(cztery) << endl;
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	cout << "Usuwanie 4:" << ts.deletee(cztery) << endl;
	
	cout << "Czy pusty:" << ts.isEmpty() << endl;

	cout << "Dodanie czterech obiektow" << endl;
	ts.add(jeden);
	ts.add(dwa);
	ts.add(trzy);
	ts.add(cztery);
	cout << "Dlugosc zbioru:" << ts.size() << endl;

	cout << "Clear:"<< endl;
	ts.clear(); 
	cout << "Czy pusty:" << ts.isEmpty() << endl;

	cout << "Dodanie czterech obiektow" << endl;
	ts.add(jeden);
	ts.add(dwa);
	ts.add(trzy);
	ts.add(cztery);
	cout << "Dlugosc zbioru:" << ts.size() << endl;

	cout << "Operator +=. Dodanie 44 do zbioru:" << endl;
	ts+=piec;
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	cout << "Czy zawiera 44:" << ts.contains(piec) << endl;

	cout << "Operator -=. Usuniecie 15 ze zbioru:" << endl;
	ts-=trzy;
	cout << "Dlugosc zbioru:" << ts.size() << endl;
	cout << "Czy zawiera 15:" << ts.contains(trzy) << endl;
	cout << "Czy zawiera 1:" << ts.contains(jeden) << endl;
	cout << "Czy zawiera 10:" << ts.contains(dwa) << endl;
	cout << "Czy zawiera 5:" << ts.contains(cztery) << endl;
	cout << "Czy zawiera 44:" << ts.contains(piec) << endl;

	TreeSet<int> ts2;
	ts2+=piec;
	ts2+=cztery;
	ts2+=jeden;
	ts2+=dwa;
	cout << "Czy ts==ts2(Powinno byc TRUE):" << ts.operator==(ts2) << endl;
	cout <<" TS:" << ts << endl;
	cout <<" TS2:" << ts2 << endl;

	TreeSet<int> ts3;
	ts3+=jeden;
	ts3+=dwa;
	ts3+=piec;
	ts3+=trzy;
	cout << endl;
	cout << "Czy ts==ts3(Powinno byc FALSE):" << ts.operator==(ts3) << endl;
	cout << "Czy ts3==ts2(Powinno byc FALSE):" << ts3.operator==(ts2) << endl;
	cout <<" TS:" << ts << endl;
	cout <<" TS2:" << ts2 << endl;
	cout <<" TS3:" << ts3 << endl;
	ts3-=jeden;
	cout << endl;
	cout << "Czy t2==ts3(Powinno byc FALSE):" << ts2.operator==(ts3) << endl;
	cout <<" TS:" << ts << endl;
	cout <<" TS2:" << ts2 << endl;
	cout <<" TS3:" << ts3 << endl;
	ts3-=trzy;
	ts3+=jeden;
	ts3+=cztery;
	cout << endl;
	cout << "Czy t2==ts3(Powinno byc TRUE):" << ts2.operator==(ts3) << endl;
	cout <<" TS:" << ts << endl;
	cout <<" TS2:" << ts2 << endl;
	cout <<" TS3:" << ts3 << endl;
	cout << endl;

	cout << "Iloczyn zbiorow TS i TS2: " << *(ts*ts2) << endl;
	TreeSet<int> ts4;
	cout <<" TS:" << ts << endl;
	cout <<" TS4:" << ts4 << endl;
	cout << "Iloczyn zbiorow TS i TS4: " << *(ts*ts4) << endl;
	TreeSet<int> ts5;
	ts5+=jeden;
	ts5+=cztery;
	int szesc = 55;
	ts5+=szesc;
	cout <<" TS:" << ts << endl;
	cout <<" TS5:" << ts5 << endl;
	cout << "Iloczyn zbiorow TS i TS5: " << *(ts*ts5) << endl;

	cout << endl;
	cout <<" TS:" << ts << endl;
	cout <<" TS2:" << ts2 << endl;
	cout <<" TS4:" << ts4 << endl;
	cout <<" TS5:" << ts5 << endl;
	cout << "Roznica zbiorow TS i TS2: " << *(ts-ts2) << endl;
	cout << "Roznica zbiorow TS i TS5: " << *(ts-ts5) << endl;
	cout << "Roznica zbiorow TS5 i TS: " << *(ts5-ts) << endl;

	cout << "Suma zbiorow TS i TS5: " << *(ts+ts5) << endl;
	cout << "Suma zbiorow TS i TS4: " << *(ts+ts4) << endl;
	cout << "Suma zbiorow TS i TS2: " << *(ts+ts2) << endl;

	int a;
	cin >> a;

}

#endif