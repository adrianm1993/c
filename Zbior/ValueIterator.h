#ifndef VALUEITERATOR_H
#define VALUEITERATOR_H
#include <iostream>   
#include <iterator>
#include "Node.h"
#include "TreeSet.h"


template<class T>
class ValueIterator{
		private:
			Node<T>* _current;
			Node<T>* _root;
		public:
			ValueIterator(Node<T>* root);
			~ValueIterator();
			void first(){this->_current = _root != nullptr ? _root->minimum() : nullptr;}
			void last(){this->_current = _root != nullptr ? _root->maximum() : nullptr;}
			bool isDone(){ return this->_current==nullptr;}
			void next();
			void previous();
			T current(){return this->_current->getValue();}
};

template<class T>
ValueIterator<T>::ValueIterator(Node<T>* root){
				this->_root=root;
				this->_current=root;
}

template<class T>
ValueIterator<T>::~ValueIterator(){
}

template<class T>
void ValueIterator<T>::next(){
	if(!this->isDone())
		this->_current = this->_current->successor();
}

template<class T>
void ValueIterator<T>::previous(){
	if(!this->isDone())
		this->_current = this->_current->predecessor();		
}

#endif