#ifndef TREESET_H
#define TREESET_H
#include "Node.h"
#include "ValueIterator.h"
#include <assert.h>
#include <string>
#include <iostream>
#include <ostream>
using namespace std;

template<class T>
class TreeSet{
	private:
		Node<T>* _root;
		int _size;

	public:
		TreeSet();
		~TreeSet();
		void setRoot(Node<T>* root);
		Node<T>* getRoot();
		bool contains(T value);
		bool add(T value);
		bool deletee(T value);
		void clear();
		int size();
		bool isEmpty();
		Node<T>* search(T value);

		void operator+= (T &value);
		void operator-= (T &value);
		bool operator== (TreeSet<T> &set2);
		TreeSet<T>* operator+ (TreeSet<T> &set2);
		TreeSet<T>* operator- (TreeSet<T> &set2);
		TreeSet<T>* operator* (TreeSet<T> &set2);
		friend ostream& operator<< (ostream &wyjscie, const TreeSet<T> &s);

};


template<class T>
TreeSet<T>::TreeSet(){
	this->_root=nullptr;
	_size=0;
}

template<class T>
TreeSet<T>::~TreeSet(){
}

template<class T>
Node<T>* TreeSet<T>::getRoot(){
	return this->_root;
}

template<class T>
void TreeSet<T>::setRoot(Node<T>* root){
	this->_root=root	
}

template<class T>
bool TreeSet<T>::contains(T value){
	return search(value) != nullptr;
}

template<class T>
bool TreeSet<T>::add(T value){
	Node<T>* parent = nullptr;
	Node<T>* node = this->_root;

	while(node != nullptr){
		parent = node;
		if(value == node->getValue())
			return false;
		
		if(value < node->getValue())
			node = node->getSmaller();
		else if(value > node->getValue())
				node = node->getLarger();
	}

	Node<T>* inserted = new Node<T>(parent, value);

	if(parent == nullptr)
		this->_root = inserted;
	else if(value < parent->getValue())
			parent->setSmaller(inserted);
		else
			parent->setLarger(inserted);

	++_size;
	return true;
}

template<class T>
bool TreeSet<T>::deletee(T value){
	Node<T>* node = search(value);
	if(node == nullptr)
		return false;
	
	Node<T>* deleted = node->getSmaller() != nullptr && node->getLarger() != nullptr ? node->successor() : node;
	
	assert (deleted!=nullptr);

	Node<T>* replacement = deleted->getSmaller() != nullptr ? deleted->getSmaller() : deleted->getLarger();

	if(replacement != nullptr)
		replacement->setParent(deleted->getParent());

	if(deleted == this->_root)
		this->_root = replacement;
	else if(deleted->isSmaller())
			deleted->getParent()->setSmaller(replacement);
		else
			deleted->getParent()->setLarger(replacement);

	if(deleted != node){
		T deletedValue = node->getValue();
		node->setValue(deleted->getValue());
		deleted->setValue(deletedValue);
	}
	
	--_size;
	return true;
}

template<class T>
void TreeSet<T>::clear(){
	this->_root=nullptr;
	this->_size=0;
}

template<class T>
int TreeSet<T>::size(){
	return this->_size;
}

template<class T>
bool TreeSet<T>::isEmpty(){
	return this->_root == nullptr;
}

template<class T>
Node<T>* TreeSet<T>::search(T value){
	Node<T>* node = this->_root;

	while(node != nullptr){
		if(value==node->getValue())
			break;
		if(value < node->getValue())
			node = node->getSmaller();
		else if(value > node->getValue())
				node = node->getLarger();
	}

	return node;
}

template<class T>
void TreeSet<T>::operator+=(T &value){
	this->add(value);
}

template<class T>
void TreeSet<T>::operator-=(T &value){
	this->deletee(value);
}

template<class T>
bool TreeSet<T>::operator==(TreeSet<T> &set2){
	Node<T>* node = this->_root;
	Node<T>* node2 = set2.getRoot();

	bool czyJest=true;
	if(_size != set2.size())
		return false;

	ValueIterator<T>* iterator = new ValueIterator<T>(node);
	iterator->first();
	while (!iterator->isDone() && czyJest==true){
		ValueIterator<T>* iterator2 = new ValueIterator<T>(node2);
		iterator2->first();
		bool pomoc=false;
		while(!iterator2->isDone()){	
			if(iterator->current()==iterator2->current())
				pomoc=true;
			if (iterator2->current() != NULL)
				iterator2->next();
		}
		if(pomoc==false)
				return false;
		if (iterator->current() != NULL)
			iterator->next();
	}

	return true;
}

template<class T>
TreeSet<T>* TreeSet<T>::operator* (TreeSet<T> &set2){
	Node<T>* node = this->_root;
	Node<T>* node2 = set2.getRoot();
	TreeSet<T>* wynik = new TreeSet<T>();

	ValueIterator<T>* iterator = new ValueIterator<T>(node);
	iterator->first();
	while (!iterator->isDone()){
		ValueIterator<T>* iterator2 = new ValueIterator<T>(node2);
		iterator2->first();
		while(!iterator2->isDone()){	
			if(iterator->current()==iterator2->current())
				wynik->add(iterator->current());
			if (iterator2->current() != NULL)
				iterator2->next();
		}
		if (iterator->current() != NULL)
			iterator->next();
	}

	return wynik;
}

template<class T>
TreeSet<T>* TreeSet<T>::operator- (TreeSet<T> &set2){
	Node<T>* node = this->_root;
	Node<T>* node2 = set2.getRoot();
	TreeSet<T>* wynik = new TreeSet<T>();

	ValueIterator<T>* iterator = new ValueIterator<T>(node);
	iterator->first();
	while (!iterator->isDone()){
		ValueIterator<T>* iterator2 = new ValueIterator<T>(node2);
		iterator2->first();
		bool pomoc=false;
		while(!iterator2->isDone()){	
			if(iterator->current()==iterator2->current())
				pomoc=true;
			if (iterator2->current() != NULL)
				iterator2->next();
		}
		if(pomoc==false)
				wynik->add(iterator->current());
		if (iterator->current() != NULL)
			iterator->next();
	}

	return wynik;
}

template<class T>
TreeSet<T>* TreeSet<T>::operator+ (TreeSet<T> &set2){
	Node<T>* node = this->_root;
	Node<T>* node2 = set2.getRoot();
	TreeSet<T>* wynik = new TreeSet<T>();

	ValueIterator<T>* iterator = new ValueIterator<T>(node);
	iterator->first();
	while (!iterator->isDone()){	
		wynik->add(iterator->current());
		if (iterator->current() != NULL)
			iterator->next();
	}

	ValueIterator<T>* iterator2 = new ValueIterator<T>(node2);
	iterator2->first();
	while(!iterator2->isDone()){
		wynik->add(iterator2->current());
		if (iterator2->current() != NULL)
			iterator2->next();
	}
		

	return wynik;
}


template<class T>
ostream & operator<< (ostream &wyjscie, TreeSet<T> &s){
	string wynik = "";
	ValueIterator<T> iterator(s.getRoot());
	iterator.first();
	while (!iterator.isDone()){
		T elem = iterator.current();
		cout << elem << " ";
		iterator.next();
	}
	return wyjscie;
}


#endif