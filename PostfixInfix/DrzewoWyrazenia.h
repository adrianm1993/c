#ifndef DrzewoWyrazenia_h
#define DrzewoWyrazenia_h
#include "TablicaSymboli.h"
#include <string>
#include <sstream>
#include <vector>

class DrzewoWyrazenia{
	private:
		string symbol;
		DrzewoWyrazenia* lewy;
		DrzewoWyrazenia* prawy;

	public :
		DrzewoWyrazenia* buduj(string odczyt,TablicaSymboli& tablica); // rozbuduj drzewo na wyrazeniu
		double obliczWartosc(TablicaSymboli& tablica);  // oblicz wartosc
		void inOrder(); //Pokaz w notacji infixowej
		void preOrder(); //Pokaz w notacji prefixowej

		DrzewoWyrazenia();
		~DrzewoWyrazenia();

		};
#endif