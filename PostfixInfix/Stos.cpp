#include "Stos.h"

Stos::Stos(){
	pierwszy=NULL;
}

Stos::~Stos(){
	if(pierwszy!=NULL)
		delete pierwszy;
}

DrzewoWyrazenia* Stos::zdejmij(){
	ElementStosu* zdjety = pierwszy;
	pierwszy=pierwszy->nastepny;
	zdjety->nastepny=NULL;
	DrzewoWyrazenia* wartosc = zdjety->elementDrzewa;
	delete zdjety;
	return wartosc;
}

void Stos::dodaj(DrzewoWyrazenia* element){
	ElementStosu* nowy = new ElementStosu(element);
	nowy->nastepny=pierwszy;
	pierwszy=nowy;
}

bool Stos::czyPusty(){
	if(pierwszy==NULL) return true;
	else return false;
}

bool Stos::czyJestJeden(){
	return pierwszy->nastepny==NULL;
}

bool Stos::czySaDwa(){
	return (pierwszy!=NULL && pierwszy->nastepny!=NULL);
}