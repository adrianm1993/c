#include "DrzewoWyrazenia.h"
#include "Stos.h"
#include <math.h>
#include <iostream>

DrzewoWyrazenia::DrzewoWyrazenia(){
	prawy=NULL;
	lewy=NULL;
}

DrzewoWyrazenia::~DrzewoWyrazenia(){
	if(lewy!=NULL)
		delete lewy;
	if(prawy!=NULL)
		delete prawy;
}

DrzewoWyrazenia* DrzewoWyrazenia::buduj(string odczyt, TablicaSymboli& tablica){
	stringstream stream(odczyt);
	string pomoc;
	Stos* stos = new Stos();
	bool czyNieMaBledow=true; //Sprawdza, czy s� b��dy

	while(stream>>pomoc && czyNieMaBledow){
		if(pomoc=="~" || pomoc=="sin" || pomoc=="cos" || pomoc=="tg" || pomoc=="ctg" || pomoc=="log"){
			if(stos->czyPusty())
				czyNieMaBledow=false;
			else{
				DrzewoWyrazenia* noweDrzewo = new DrzewoWyrazenia();
				noweDrzewo->symbol=pomoc;
				noweDrzewo->lewy=stos->zdejmij();
				stos->dodaj(noweDrzewo);
			}

		}
		else if(pomoc=="+" || pomoc=="-" || pomoc=="*" || pomoc=="/"){
			if(stos->czySaDwa()==false)
				czyNieMaBledow=false;
			else{
				DrzewoWyrazenia* noweDrzewo = new DrzewoWyrazenia();
				noweDrzewo->symbol=pomoc;
				noweDrzewo->lewy=stos->zdejmij();
				noweDrzewo->prawy=stos->zdejmij();
				stos->dodaj(noweDrzewo);
			}
		}
		else{
			if(pomoc.at(0)>='a'&& pomoc.at(0)<='z'){ //Je�eli jest zmienna.
				if(tablica.zawiera(pomoc.at(0))==false)
					tablica.dodajSymbol(pomoc.at(0),NULL);
				DrzewoWyrazenia* noweDrzewo = new DrzewoWyrazenia();
				noweDrzewo->symbol=pomoc;
				stos->dodaj(noweDrzewo);
			}
			else if(pomoc.at(0)>48 && pomoc.at(0)<58){ //Je�eli jest liczba.
				DrzewoWyrazenia* noweDrzewo = new DrzewoWyrazenia();
				noweDrzewo->symbol=pomoc;
				stos->dodaj(noweDrzewo);
			}
			else
				czyNieMaBledow=false;
		}
	}

	if(czyNieMaBledow && stos->czyJestJeden()){
		DrzewoWyrazenia* korzen = stos->zdejmij();
		delete stos;
		return korzen;
	}
	else{
		delete stos;
		return NULL;
	}
}

void DrzewoWyrazenia::inOrder(){
	if(lewy==NULL && prawy==NULL)
		cout << symbol;
	else{
		cout << "(";
		if(prawy!=NULL)
			prawy->inOrder();
		cout << symbol;
		if(lewy!=NULL)
			lewy->inOrder();
		cout << ")";
	}
}

void DrzewoWyrazenia::preOrder(){
	if(lewy==NULL && prawy==NULL)
		cout << symbol;
	else{
		cout << symbol << " ";
		if(prawy!=NULL)
			prawy->preOrder();
		cout << " ";
		if(lewy!=NULL)
			lewy->preOrder();
		cout << " ";
	}

}

double DrzewoWyrazenia::obliczWartosc(TablicaSymboli& tablica){
	stringstream ss;
	double wartosc;

	if(lewy==NULL && prawy==NULL){
		if(symbol.at(0)>='a'&& symbol.at(0)<='z')  // w lisciu jest zmienna
			return tablica.dajWartoscZ(symbol.at(0));
		else{
			ss<<symbol;
			ss>>wartosc;
			return wartosc;
		}
	}
	else{
		if(symbol=="~")
			return -lewy->obliczWartosc(tablica);
		if(symbol=="sin")
			return sin(lewy->obliczWartosc(tablica));
		if(symbol=="cos")
			return cos(lewy->obliczWartosc(tablica));
		if(symbol=="tg")
			return tan(lewy->obliczWartosc(tablica));
		if(symbol=="log")
			return log(lewy->obliczWartosc(tablica));
		if(symbol=="ctg")
			return 1/(tan(lewy->obliczWartosc(tablica)));

		if(symbol=="+")
			return (prawy->obliczWartosc(tablica) + lewy->obliczWartosc(tablica));	
		if(symbol=="-")
			return (prawy->obliczWartosc(tablica) - lewy->obliczWartosc(tablica));
		if(symbol=="*")
			return (prawy->obliczWartosc(tablica) * lewy->obliczWartosc(tablica));
		if(symbol=="/")
			return (prawy->obliczWartosc(tablica) / lewy->obliczWartosc(tablica));		
	}		

}