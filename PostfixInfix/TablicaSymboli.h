#ifndef TablicaSymboli_h
#define TablicaSymboli_h

#include"Symbol.h"
#include<vector>
using namespace std;

class TablicaSymboli{
	private:
		vector<Symbol> tablica;

	public:
		TablicaSymboli();
		~TablicaSymboli();
		double dajWartoscZ(char symbol);
		bool zawiera(char symbol);
		void usunWszystko();
		void zmienWartosc(char symbol, double wartosc);
		void dodajSymbol(char symbol, double wartosc);
		bool czyZdefiniowanyS(char symbol);
		bool czyZdefiniowanyI(int index);
		bool czyPusta();
		int dlugosc();
		char dajSymbolZ(int index);	
};
#endif