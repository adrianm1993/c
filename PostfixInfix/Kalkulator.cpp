#include "Kalkulator.h"
#include <iostream>
#define komunikatBledu "Nieprawidlowe dane"
#define komunikatDeklaracji "Podaj wartosc zmiennej "

Kalkulator::Kalkulator(){
	drzewo= new DrzewoWyrazenia();
	tablica = new TablicaSymboli();
}

Kalkulator::~Kalkulator(){
	delete drzewo;
	delete tablica;
}

bool Kalkulator::zaladuj(string odczyt)
{
	double wartosc;
	delete drzewo;
	drzewo = new DrzewoWyrazenia();

	drzewo= drzewo->buduj(odczyt,*tablica); // rozbuduj drzewo na wyrazeniu ( w razie bledu zwraca NULL, w razie powodzenia wskaznik na korzen)

	if(drzewo==NULL){
		cout<< komunikatBledu <<endl;
		return false;
	}

	// Nadanie wartosci niezdefiniowanym zmiennym
	for(int i=0;i<tablica->dlugosc();i++){
		if(tablica->czyZdefiniowanyI(i)==false){
			cout<< komunikatDeklaracji <<tablica->dajSymbolZ(i)<<endl;
			cin>>wartosc;
			tablica->zmienWartosc(tablica->dajSymbolZ(i),wartosc);
			system("cls"); //Wyczyszczenie ekranu
		}
	}
	return true;
}

void Kalkulator::pokazJakoInfix(){
	drzewo->inOrder();
}

void Kalkulator::pokazJakoPrefix(){
	drzewo->preOrder();
}

double Kalkulator::obliczWyrazenie(){
	return drzewo->obliczWartosc(*tablica);
}

void Kalkulator::dodajSymbol(char symbol, double wartosc){
	if(tablica->zawiera(symbol))
		tablica->zmienWartosc(symbol,wartosc);
	else
		tablica->dodajSymbol(symbol,wartosc);
}

