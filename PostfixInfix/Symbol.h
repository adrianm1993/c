#ifndef Symbol_h
#define Symbol_h
class Symbol{
	private:
		double wartosc;
		char symbol;

	public:
		double dajWartosc();
		void ustawWartosc(double war);
		char dajSymbol();
		Symbol(char sym, double war);
};
#endif