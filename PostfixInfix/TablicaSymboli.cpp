#include "TablicaSymboli.h"

TablicaSymboli::TablicaSymboli(){
	tablica.reserve(10); //Rezerwuje miejsce w kontenerze.
}
double TablicaSymboli::dajWartoscZ(char symbol){ //Zwraca warto�� dla okre�lonego symbolu z tablicy.
	int i;
	for(i=0; tablica.at(i).dajSymbol()!=symbol; i++);

	return tablica.at(i).dajWartosc();
}

bool TablicaSymboli::zawiera(char symbol){
	if(tablica.size()==0)
		return false;
	else{
		int i;
		for(i=0; i<tablica.size() && tablica.at(i).dajSymbol()!=symbol ; i++);

		if(i==tablica.size())
			i--;
		return tablica.at(i).dajSymbol()==symbol;
	}
}

void TablicaSymboli::usunWszystko(){
	tablica.clear();
}

void TablicaSymboli::zmienWartosc(char symbol,double wartosc){
	int i;
	for(i=0; tablica.at(i).dajSymbol()!=symbol; i++);

	tablica.at(i).ustawWartosc(wartosc);
}

void TablicaSymboli::dodajSymbol(char symbol, double wartosc){
	Symbol nowy =  Symbol(symbol,wartosc);
	tablica.push_back(nowy);
	tablica.shrink_to_fit(); //Funkcja ta dopasowuje ilosc zarezerwowanej pami�ci do potrzebnej wielko�ci.
}

bool TablicaSymboli::czyZdefiniowanyS(char symbol){
	int i;
	for( i=0; tablica.at(i).dajSymbol()!=symbol; i++);

	return tablica.at(i).dajWartosc()!=NULL;
}

bool TablicaSymboli::czyZdefiniowanyI(int index){
	return tablica.at(index).dajWartosc()!=NULL;
}

bool TablicaSymboli::czyPusta(){
	return tablica.size()==0;
}

int TablicaSymboli::dlugosc(){
	return tablica.size();
}

char TablicaSymboli::dajSymbolZ(int index){
	return tablica.at(index).dajSymbol();
}

TablicaSymboli::~TablicaSymboli(){
	usunWszystko();
}
