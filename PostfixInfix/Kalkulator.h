#include "TablicaSymboli.h"
#include "DrzewoWyrazenia.h"

class Kalkulator
{
	private:
		DrzewoWyrazenia* drzewo;
		TablicaSymboli* tablica;

	public:
		Kalkulator();
		~Kalkulator();
		bool zaladuj(string odczyt); // zaladuj wyrazenie
		void pokazJakoInfix();				// pokaz jako infix
		void pokazJakoPrefix();
		double obliczWyrazenie();		// oblicz wyrazenie
		void dodajSymbol(char symbol,double wartosc);  //(dodaj / zamienien nowa zmienna)
};