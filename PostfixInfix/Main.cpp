#include "Kalkulator.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <math.h>

#define komunikat "Wpisz wyrazenie w notacji POSFIXOWEJ:"
#define komunikatMenu1 "Co chcesz wykonac:"
#define komunikatMenu2 "1-Pokaz wyrazenie w postaci INFIXOWEJ"
#define komunikatMenu3 "2-Oblicz wyrazenie"
#define komunikatMenu4 "3-Wpisz nowe wyrazenie"
#define komunikatMenu5 "4-Dodaj nowa zmienna"
#define komunikatMenu6 "5-Pokaz wyrazenie w postaci PREFIXOWEJ"
#define komunikatMenu7 "0-Zakoncz program"
#define komunikatDodaniaSymbolu "Podaj nowa zmienna"
#define komunikatDodaniaWartosciSymbolu "Podaj wartosc tej zmiennej"

void main(){
	Kalkulator kalkulator = Kalkulator();
	string pomoc;

	do{
		cout << komunikat << endl;	
		getline(cin,pomoc);
		system("cls");
	}
	while (kalkulator.zaladuj(pomoc)==false);

	int liczba=100;
	while(liczba!=0){
		cout <<endl;
		cout << komunikatMenu1 << endl;
		cout << komunikatMenu2 << endl;
		cout << komunikatMenu3 << endl;
		cout << komunikatMenu4 << endl;
		cout << komunikatMenu5 << endl;
		cout << komunikatMenu6 << endl;
		cout << komunikatMenu7 << endl;

		cin >> liczba;

		if(liczba==1){
			cout <<endl;
			kalkulator.pokazJakoInfix();
			cout <<endl;
		}
		if(liczba==2){
			cout <<endl;
			cout << kalkulator.obliczWyrazenie();
			cout <<endl;
		}
		if(liczba==3){
			cin.get();
			do{
				system("cls");
				cout<< komunikat <<endl;
				getline(cin,pomoc);
			}while(kalkulator.zaladuj(pomoc)==false);
			system("cls");
		}
		if(liczba==4){
			char symbol;
			double wartosc;
			cout << endl;
			cout<< komunikatDodaniaSymbolu <<endl;
			cin>>symbol;
			cout << endl;
			cout<< komunikatDodaniaWartosciSymbolu <<endl;
			cin>>wartosc;
			cout << endl;
			kalkulator.dodajSymbol(symbol,wartosc);
		}
		if(liczba==5){
			cout <<endl;
			kalkulator.pokazJakoPrefix();
			cout <<endl;
		}
	}
}