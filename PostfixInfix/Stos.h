#include "DrzewoWyrazenia.h"
#include "ElementStosu.h"
class Stos{
	private:
		ElementStosu* pierwszy;		
	public:
		Stos();
		~Stos();
		DrzewoWyrazenia* zdejmij();
		void dodaj(DrzewoWyrazenia* element);
		bool czyPusty();
		bool czyJestJeden();
		bool czySaDwa();			
};