#pragma once
#include "stdafx.h"
#include "Graph.h"
#include <sstream>

using namespace std;

Graph::Graph() : graph(10)
{
	this->createGraphFromTextFile();

	set<int> *tM = new set<int>[nodes + 1];

	for (int i = 1; i <= this->nodes; i++)
		for (unsigned int j = 0; j < this->tempGraph[i].size(); j++)
			tM[i].insert(tempGraph[i][j]);

	for (int i = 1; i <= this->nodes; i++)
		this->tempGraph[i].clear();

	for (int i = 1; i <= this->nodes; i++)
		for (set<int>::iterator it = tM[i].begin(); it != tM[i].end(); ++it)
			this->tempGraph[i].push_back(*it);

	graph.resize(nodes + 1);

	for (int i = 1; i <= this->nodes; i++)
	{
		vector<int> oneRow(tempGraph[i].size());
		for (unsigned int j = 0; j < this->tempGraph[i].size(); j++)
		{
			oneRow.at(j) = tempGraph[i][j];
		}
		this->graph[i] = oneRow;
	}
}

void Graph::createGraphFromTextFile()
{
	ifstream file;
	string fileName = file_1;
	file.open(fileName.c_str());

	//Odczytanie pierwszego wiersza z pliku
	string line;
	getline(file, line);
	cout << line << endl;
	//-----------------------------------//

	//Podzia� pierwszego wiersza i otrzymanie liczy w�z��w i kraw�dzi
	std::stringstream ss;
	ss << line;
	string temp1, temp2;
	ss >> temp1 >> temp2 >> this->nodes >> this->edges;
	//-------------------------------------------------------------//

	this->tempGraph = new vector<int>[nodes + 1];

	//Wczytanie kraw�dzi do listy incydencji
	while (!file.eof())
	{
		getline(file, line);
		std::stringstream ss;
		ss << line;
		int node;
		int edge;
		string temp1;
		ss >> temp1 >> node >> edge;

		tempGraph[node].push_back(edge);
		tempGraph[edge].push_back(node);
	}
	//------------------------------------//

	file.close();
}

Graph::~Graph()
{
	
}

void Graph::getView(){
	for (int i = 1; i <= this->nodes; i++)
	{
		cout << endl << i << ": ";
		for (unsigned int j = 0; j < this->graph[i].size(); j++)
			cout << graph[i][j] << " ";
	}
}


void Graph::createFirstPopulation()
{
	vector<int> onePerson(this->nodes);

	int color;

	for (int i = 0; i < populations; i++)
	{
		for (int j = 0; j < this->nodes; j++)
		{
			color = rand() % colors;
			onePerson.at(j) = color;
		}
		this->population.push_back(onePerson);
	}
}

void Graph::viewPopulation()
{
	for (int i = 0; i < populations; i++)
	{
		cout << i << ":";
		for (int j = 0; j < this->nodes; j++)
			cout << population[i][j] << " ";
		cout << endl;
	}
}

void Graph::individualErrors()
{
	int errors;
	for (unsigned int i = 0; i < this->population.size(); i++)
	{
		errors = 0;
		for (int j = 1; j <= this->nodes; j++)
		{
			int color = this->population[i][j - 1];
			for (unsigned int k = 0; k < this->graph[j].size(); k++)
			{
				int temp = this->graph[j][k];
				int temp2 = this->population[i][temp - 1];

				if (color == this->population[i][temp - 1])
					errors++;
			}
		}
		this->errors.push_back(errors);
	}
}

void Graph::tournamentSelection()
{
	for (int i = 0; i < percentOfPopulations; i++)
	{
		int personNumber;
		personNumber = rand() % populations;
		tab[i] = personNumber;
	}	
}

vector<int> Graph::getWinnerOfTournament()
{
	vector<int> best(this->nodes);

	for (int i = 0; i < percentOfPopulations; i++)
		this->tournamentGroup.push_back(population[tab[i]]);

	int min = this->errors[tab[0]];
	int bestIndividual = 0;
	for (int i = 0; i < percentOfPopulations; i++)
		if (this->errors[tab[i]] < min)
		{
			min = this->errors[tab[i]];
			bestIndividual = i;
		}

	for (int i = 0; i < this->nodes; i++)
		best.at(i) = this->tournamentGroup[bestIndividual][i];

	return best;
}

vector<vector<int>> Graph::crucifixion(vector<int> firstVector, vector<int> secondVector)
{
	vector<int> first(this->nodes);
	vector<int> second(this->nodes);
	vector<int> tempFirst(this->nodes);
	vector<int> tempSecond(this->nodes);
	int middle = this->nodes / 2;

	for (unsigned int i = 0; i < firstVector.size(); i++)
		first.at(i) = firstVector[i];
	for (unsigned int i = 0; i < secondVector.size(); i++)
		second.at(i) = secondVector[i];

	int percent = rand() % percentOfCrucfixion + 1;
	
	if (percent == 2)
	{
		for (int i = 0; i < this->nodes; i++)
		{
			if (i <= middle)
			{
				tempFirst.at(i) = first[i];
				tempSecond.at(i) = second[i];
			}
			else
			{
				tempFirst.at(i) = second.at(i);
				tempSecond.at(i) = first.at(i);
			}
		}

		first.empty();
		second.empty();
		
		for (int i = 0; i < this->nodes; i++)
		{
			first.at(i) = tempFirst[i];
			second.at(i) = tempSecond[i];
		}
	}

	vector<vector<int>> toReturn;
	toReturn.push_back(first);
	toReturn.push_back(second);
	return toReturn;
}

vector<vector<int>> Graph::mutation(vector<vector<int>> firstAndSecond)
{
	vector<int> first(this->nodes);
	vector<int> second(this->nodes);

	for (unsigned int i = 0; i < firstAndSecond[0].size(); i++)
		first.at(i) = firstAndSecond[0][i];
	for (unsigned int i = 0; i < firstAndSecond[1].size(); i++)
		second.at(i) = firstAndSecond[1][i];

	for (int i = 0; i < this->nodes; i++)
	{
		int percent = rand() % percentOfMutaton + 1;
		if (percent <= 1)
		{
			first.at(i) = rand() % colors;
			second.at(i) = rand() % colors;
		}
	}

	vector<vector<int>> toReturn;
	toReturn.push_back(first);
	toReturn.push_back(second);
	return toReturn;
}


void Graph::AG()
{
	vector<int> first(this->nodes);
	vector<int> second(this->nodes);

	tournamentSelection();
	first = this->getWinnerOfTournament();

	for (int i = 0; i < percentOfPopulations; i++)
		this->tournamentGroup.clear();

	tournamentSelection();
	second = this->getWinnerOfTournament();

	vector<vector<int>> temp = crucifixion(first, second);
	vector<vector<int>> temp2 = mutation(temp);	

	this->newPopulation.push_back(temp2[0]);
	this->newPopulation.push_back(temp2[1]);
}

void Graph::AG100()
{
	individualErrors();

	for (int i = 0; i < 50; i++)
		AG();

	this->informations.push_back(errors);

	for (int i = 0; i < populations; i++)
		this->population[i].clear();

/*	cout << endl << endl;
	for (int i = 0; i < this->informations.size(); i++)
		for (int j = 0; j < this->informations[i].size(); j++)
			cout << this->informations[i][j];
	cout << endl;
*/
	population = newPopulation;
//	cout << endl << endl;
//	viewPopulation();

	newPopulation.clear();
	errors.clear();
}

void Graph::AG100100()
{
	for (int i = 0; i < 100; i++)
	{
		AG100();
	}
}