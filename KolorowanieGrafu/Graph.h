#pragma once
#include "stdafx.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstdlib>
#include <time.h>
#include <set>

using namespace std;

const string file_1 = "testyGraf.txt";
const int colors = 5;  //Zadana ilo�� kolor�w
const int populations = 100; //Ilo�� osobnik�w w jednej populacji
const int percentOfPopulations = 5;  //Oke�lenie ilosci osobnik�w w pojedy�czym turnieju
const int percentOfCrucfixion = 2; //rand()%2+1 - 50% - Pstwo krzy�owania
const int percentOfMutaton = 100;  //rand()%100+1 - 1% - Pstwo mutacji

class Graph{
private:
	int nodes = 0;
	int edges = 0;
	vector<int> *tempGraph;
	int tab[percentOfPopulations];

public:	
	vector<vector<int>> graph;
	vector<vector<int>> population;
	vector<vector<int>> newPopulation;
	vector<vector<int>> tournamentGroup; 
	vector<int> errors;
	vector<vector<int>> informations;
	

	Graph();
	~Graph();
	void getView();
	void createGraphFromTextFile();
	void createFirstPopulation();
	void viewPopulation();
	void tournamentSelection();
	vector<int> getWinnerOfTournament();
	vector<vector<int>> crucifixion(vector<int> first, vector<int> second);
	vector<vector<int>> mutation(vector<vector<int>> firstAndSecond);
	void individualErrors();
	void AG();
	void AG100();
	void AG100100();
};