// ProblemHetmanow.cpp : main project file.

#include "stdafx.h"
#include <iostream>
#include "QueensPuzzle.h"

using namespace System;
using namespace std;



int main(array<System::String ^> ^args)
{

	cout << "BACKTRACKING:";
	QueensPuzzle queensPuzzle;
	queensPuzzle.doOLD();

	cout << endl << endl << "BACKTRACKING 2:";
	queensPuzzle.doBacktracking();
	
	cout << endl << endl << "FORWARD CHECKING:";
	queensPuzzle.doForward();


	int cancel;
	cin >> cancel;
    return 0;
}
