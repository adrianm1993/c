#pragma once
#include "stdafx.h"
#include <iostream>
#include <vector>

using namespace std;

const int size =8; //Rozmiar tablicy SIZE X SIZE oraz ilo�� hetman�w do rozmieszczenia na planszy.

class QueensPuzzle{
	private:
		int visitedBack = 0;
		int visitedBack2 = 0;
		int visitedForward = 0;
		vector<vector<int>> output; //Wynik dla BACKTRACKINGu w wersji starej.
		vector<vector<int>> output2; //Wynik dla BACKTRACKINGu w wersji nowej.
		vector<vector<int>> output3; //Wynik dla FORWARD CHECKING.

	public:
		QueensPuzzle();
		~QueensPuzzle();

		void getResultView(vector<int> results);
		void getResultView2(int results2[size][size]);
		void getResultView3(vector<vector<int>> board);

	
		//Backtracking spos�b pierwszy
		bool backtrackingOLD(vector<int> &results, int col);
		bool isGoodOLD(vector<int> results, int col, int row);
		void doOLD();

		//Backtracking spos�b nowy.
		bool backtracking(vector<vector<int>> &board, vector<int> &chessA, int col);
		bool isGood(vector<vector<int>> &board, vector<int> &chessA, int row, int col);
		bool doBacktracking();

		//Forward checking.
		bool checkForward(vector<vector<int>> &board, vector<int> &chessB, int localRow, int localCol);
		bool restore(vector<vector<int>> &board, int localCol);
		bool FC(vector<vector<int>> &board, vector<int> &chessB, int localCol);
		void doForward();
};

