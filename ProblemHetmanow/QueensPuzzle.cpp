#pragma once
#include "stdafx.h"
#include "QueensPuzzle.h"
#include<stdio.h>
#include <time.h>

using namespace std;

QueensPuzzle::QueensPuzzle()
{	
}

QueensPuzzle::~QueensPuzzle()
{

}

void QueensPuzzle::getResultView(vector<int> results)
{
	cout << endl << "[";
	for (unsigned int i = 0; i < size; i++)
		printf(" %d ", results[i]);
	cout << "]" << endl;
}

void QueensPuzzle::getResultView2(int board[size][size])
{
	for (unsigned int i = 0; i < size; i++)
	{
		for (unsigned int j = 0; j < size; j++)
			printf(" %d ", board[i][j]);
		printf("\n");
	}
}

void QueensPuzzle::getResultView3(vector<vector<int>> board)
{
	for (unsigned int i = 0; i <board.size(); i++)
	{
		printf("%d: [", i+1);
		for (unsigned int j = 0; j < board[i].size(); j++)
			printf(" %d ", board[i][j]);
		printf("]\n");
	}
}



bool QueensPuzzle::backtrackingOLD(vector<int> &results, int localCol)
{	
	if (localCol == size)
	{
		output.push_back(results);
		return false;
	}

	for (int i = 0; i < size; i++)
	{
		visitedBack++;
		if (this->isGoodOLD(results, i, localCol))
		{
			results[localCol] = i;
			bool result = backtrackingOLD(results, localCol + 1);
			if (result)
				return result;
			results[localCol] = -1; 
		}
	}

	return false;
}

bool QueensPuzzle::isGoodOLD(vector<int> results, int localRow, int localCol)
{
	for (int i = 0; i < localCol; i++)
		if (i == localCol || results[i] == localRow || abs(i - localCol) == abs(results[i] - localRow))
			return false;

	return true;
}

void QueensPuzzle::doOLD()
{
	vector<int> results(size);

	for (int i = 0; i < size; i++)
		results[i] = -1;

	clock_t start, koniec;
	start = clock();
	backtrackingOLD(results, 0);
	koniec = clock();
	cout << "Czas trwania BACKTRACKING: " << koniec - start << endl;
	cout << "Odwiedzone wierzcholki BACKTRACKING: " << visitedBack << endl;
//	getResultView3(output);
}



bool QueensPuzzle::isGood(vector<vector<int>> &board, vector<int> &chessA, int localRow, int localCol)
{
	int i, j;

	/* Check this row on left side */
	for (i = 0; i < localCol; i++)
	{
		if (board[localRow][i])
			return false;
	}

	/* Check upper diagonal on left side */
	for (i = localRow, j = localCol; i >= 0 && j >= 0; i--, j--)
	{
		if (board[i][j])
			return false;
	}

	/* Check lower diagonal on left side */
	for (i = localRow, j = localCol; j >= 0 && i < size; i++, j--)
	{
		if (board[i][j])
			return false;
	}

	return true;
}

bool QueensPuzzle::backtracking(vector<vector<int>> &board, vector<int> &chessA, int localCol)
{
	if (localCol == size){
		output2.push_back(chessA);
		return false;
	}

	for (int i = 0; i < size; i++)
	{
		visitedBack2++;
		if (isGood(board, chessA, i, localCol))
		{
			board[i][localCol] = 1;
			chessA[localCol] = i;
			if (backtracking(board, chessA, localCol + 1) == true)
				return true;
			board[i][localCol] = 0;
		}
	}
	return false;
}

bool QueensPuzzle::doBacktracking()
{
	vector<vector<int>> board;
	vector<int> temp(size);
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
			temp[j] = 0;
		board.push_back(temp);
	}

	vector<int> chessA(size);
	for (int i = 0; i < size; i++)
		chessA[i] = -1;

	clock_t start, koniec;
	start = clock();
	backtracking(board, chessA, 0);
	koniec = clock();
	cout << "Czas trwania BACKTRACKING: " << koniec - start << endl;
	cout << "Odwiedzone wierzcholki BACKTRACKING2: " << visitedBack2 << endl;
//	getResultView3(output2);
	return true;
}




bool QueensPuzzle::checkForward(vector<vector<int>> &board, vector<int> &chessB, int localRow, int localCol)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			if ((i == localCol || j == localRow || abs(i - localCol) == abs(j - localRow)) && board[i][j] == -1)
				board[i][j] = localCol;

	return true;
}

bool QueensPuzzle::restore(vector<vector<int>> &board, int localCol)
{
	for (int i = 0; i < board.size(); i++)
		for (int j = 0; j < board.size(); j++)
			if (board[i][j] == localCol)
				board[i][j] = -1;

	return true;
}

bool QueensPuzzle::FC(vector<vector<int>> &board, vector<int> &chessB, int localCol)
{
	if (localCol == size){
		output3.push_back(chessB);
		return false;
	}

	this->visitedForward++;

	for (unsigned int i = 0; i < size; i++)
	{
		if (board[localCol][i] == -1)
		{
			checkForward(board, chessB, i, localCol);

			chessB[localCol] = i;
			bool result = FC(board, chessB, localCol + 1);
			if (result)
				return true;

			restore(board, localCol);
		}
		
	}

	return false;
}

void QueensPuzzle::doForward()
{
	vector<vector<int>> board;
	vector<int> temp(size);
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
			temp[j] = -1;
		board.push_back(temp);
	}

	vector<int> chessB(size);
	for (int i = 0; i < size; i++)
		chessB[i] = -1;

	clock_t start, koniec;
	start = clock();
	FC(board, chessB, 0);
	koniec = clock();
	cout << "Czas trwania FORWARD CHECKING: " << koniec - start << endl;
	cout << "Odwiedzone wierzcholki FORWARD CHECKING: " << visitedForward << endl;
//	getResultView3(output3);
}